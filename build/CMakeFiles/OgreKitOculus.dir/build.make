# CMAKE generated file: DO NOT EDIT!
# Generated by "MinGW Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

SHELL = cmd.exe

# The CMake executable.
CMAKE_COMMAND = "C:\Program Files (x86)\CMake 2.8\bin\cmake.exe"

# The command to remove a file.
RM = "C:\Program Files (x86)\CMake 2.8\bin\cmake.exe" -E remove -f

# Escaping for special characters.
EQUALS = =

# The program to use to edit the cache.
CMAKE_EDIT_COMMAND = "C:\Program Files (x86)\CMake 2.8\bin\cmake-gui.exe"

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = C:\vr\OgreKitOculus

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = C:\vr\OgreKitOculus\build

# Include any dependencies generated for this target.
include CMakeFiles/OgreKitOculus.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/OgreKitOculus.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/OgreKitOculus.dir/flags.make

CMakeFiles/OgreKitOculus.dir/main.cpp.obj: CMakeFiles/OgreKitOculus.dir/flags.make
CMakeFiles/OgreKitOculus.dir/main.cpp.obj: CMakeFiles/OgreKitOculus.dir/includes_CXX.rsp
CMakeFiles/OgreKitOculus.dir/main.cpp.obj: ../main.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report C:\vr\OgreKitOculus\build\CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/OgreKitOculus.dir/main.cpp.obj"
	c:\MinGW\bin\g++.exe   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles\OgreKitOculus.dir\main.cpp.obj -c C:\vr\OgreKitOculus\main.cpp

CMakeFiles/OgreKitOculus.dir/main.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/OgreKitOculus.dir/main.cpp.i"
	c:\MinGW\bin\g++.exe  $(CXX_DEFINES) $(CXX_FLAGS) -E C:\vr\OgreKitOculus\main.cpp > CMakeFiles\OgreKitOculus.dir\main.cpp.i

CMakeFiles/OgreKitOculus.dir/main.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/OgreKitOculus.dir/main.cpp.s"
	c:\MinGW\bin\g++.exe  $(CXX_DEFINES) $(CXX_FLAGS) -S C:\vr\OgreKitOculus\main.cpp -o CMakeFiles\OgreKitOculus.dir\main.cpp.s

CMakeFiles/OgreKitOculus.dir/main.cpp.obj.requires:
.PHONY : CMakeFiles/OgreKitOculus.dir/main.cpp.obj.requires

CMakeFiles/OgreKitOculus.dir/main.cpp.obj.provides: CMakeFiles/OgreKitOculus.dir/main.cpp.obj.requires
	$(MAKE) -f CMakeFiles\OgreKitOculus.dir\build.make CMakeFiles/OgreKitOculus.dir/main.cpp.obj.provides.build
.PHONY : CMakeFiles/OgreKitOculus.dir/main.cpp.obj.provides

CMakeFiles/OgreKitOculus.dir/main.cpp.obj.provides.build: CMakeFiles/OgreKitOculus.dir/main.cpp.obj

# Object files for target OgreKitOculus
OgreKitOculus_OBJECTS = \
"CMakeFiles/OgreKitOculus.dir/main.cpp.obj"

# External object files for target OgreKitOculus
OgreKitOculus_EXTERNAL_OBJECTS =

OgreKitOculus.exe: CMakeFiles/OgreKitOculus.dir/main.cpp.obj
OgreKitOculus.exe: CMakeFiles/OgreKitOculus.dir/build.make
OgreKitOculus.exe: ../../glew/lib/libglew32.a
OgreKitOculus.exe: CMakeFiles/OgreKitOculus.dir/objects1.rsp
OgreKitOculus.exe: CMakeFiles/OgreKitOculus.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable OgreKitOculus.exe"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles\OgreKitOculus.dir\link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/OgreKitOculus.dir/build: OgreKitOculus.exe
.PHONY : CMakeFiles/OgreKitOculus.dir/build

CMakeFiles/OgreKitOculus.dir/requires: CMakeFiles/OgreKitOculus.dir/main.cpp.obj.requires
.PHONY : CMakeFiles/OgreKitOculus.dir/requires

CMakeFiles/OgreKitOculus.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles\OgreKitOculus.dir\cmake_clean.cmake
.PHONY : CMakeFiles/OgreKitOculus.dir/clean

CMakeFiles/OgreKitOculus.dir/depend:
	$(CMAKE_COMMAND) -E cmake_depends "MinGW Makefiles" C:\vr\OgreKitOculus C:\vr\OgreKitOculus C:\vr\OgreKitOculus\build C:\vr\OgreKitOculus\build C:\vr\OgreKitOculus\build\CMakeFiles\OgreKitOculus.dir\DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/OgreKitOculus.dir/depend

